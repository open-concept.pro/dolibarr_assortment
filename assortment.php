<?php
/* Copyright (C) 2011-2013 Florian HENRY  <florian.henry@open-concept.pro>
 *
 * Code of this page is mostly inspired from module category
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/**
 *  \file       assortment/assortment.php
 *  \ingroup    crm
 *  \brief      Set Assortment by product or customer pages
 */


$res=@include("../main.inc.php");					// For root directory
if (! $res) $res=@include("../../main.inc.php");	// For "custom" directory
if (! $res) die("Include of main fails");

require_once "class/assortment.class.php";
require_once "lib/assortment.lib.php";


// Load traductions files requiredby by page
$langs->load("assortment@assortment");

if ($conf->global->ASSORTMENT_BY_CAT == 1) {
	$langs->load("categorie");
}

//All page variable
$mesgErr='';
$mesg='';

// Get parameters
$b_isAssortProd = false;
$b_isAssortThirdParty=false;

$id=GETPOST("id",'int');
$action=GETPOST('action','alpha');
$type=GETPOST('type','alpha');
$prodid=GETPOST('prodid','int');
$idprod=GETPOST('idprod','int');
$ref=GETPOST('ref','alpha');
$socid=GETPOST("socid",'int');
$typeaction=GETPOST('typeaction','alpha');
$catMere=GETPOST('catMere','int');
$productid=GETPOST('productid','int');
$catMereRemProd=GETPOST('catMereRemProd');
$catMereRemCust=GETPOST('catMereRemCust');
$catMereRemSupp=GETPOST('catMereRemSupp');


if ($type==0){
	$b_isAssortProd = true;
}
if ($type==1){
	$b_isAssortThirdParty = true;
}


// Security check
if ($user->right->assortment->lire) $socid=$user->societe_id;
$result = restrictedArea($user,'assortment',$user->societe_id);


/*******************************************************************
* ACTIONS
*
* Put here all code to do according to value of "action" parameter
********************************************************************/
if ($action == "AddAssort") {
	if ($user->rights->assortment->creer) {	
		if ($conf->global->ASSORTMENT_BY_CAT == 1 && $typeaction=='cat') {
			require_once(DOL_DOCUMENT_ROOT."/categories/class/categorie.class.php");
			
			$b_err=false;	
			
			$ObjectByCateg=new Assortment($db);
			
			if ($b_isAssortThirdParty) {
				require_once(DOL_DOCUMENT_ROOT."/product/class/product.class.php");
				
				//Retreive all product link to category
				$prods=$ObjectByCateg->get_all_prod_by_categ($catMere);
				
				foreach($prods as &$val) {
					$assort=new Assortment($db);
					$assort->label=$socid.'_Cat_'.$catMere;
					$assort->fk_soc=$socid;
					$assort->fk_prod=$val;
					$result=$assort->create($user);
	
					if ($result <= 0) {
						// Creation KO
						$b_err=true;
						//Retreive product name
						$prod = new Product($db);
						$prod->fetch($val);
						setEventMessage($langs->trans("ProdWasNotAddedSuccessfully",$prod->ref.' '.$prod->libelle),'errors');
					}
				}		
				if (!$b_err) {
					//Retreive category name
					$cat = new Categorie($db);
					$cat->fetch($catMere);
				
					setEventMessage($langs->trans("ProdWasAddedSuccessfullyForCat",$cat->label),'mesgs');
				}
			}
			if ($b_isAssortProd) {
				require_once(DOL_DOCUMENT_ROOT."/societe/class/societe.class.php");
				
				//Retreive all product link to category
				$customers=$ObjectByCateg->get_all_customer_by_categ($catMere);
				
				foreach($customers as &$val) {
					$assort=new Assortment($db);
					$assort->label=$val.'_Cat_'.$catMere;
					$assort->fk_soc=$val;
					$assort->fk_prod=$prodid;
					$result=$assort->create($user);
	
					if ($result <= 0) {
						// Creation KO
						$b_err=true;
						//Retreive thirdparty name
						$Customer = new Societe($db);
						$Customer->fetch($val);

						setEventMessage($langs->trans("CustomerWasNotAddedSuccessfully",$Customer->nom),'errors');
					}
				}		
				if (!$b_err) {
					//Retreive category name
					$cat = new Categorie($db);
					$cat->fetch($catMere);
					setEventMessage($langs->trans("CustWasAddedSuccessfullyForCat",$cat->label),'mesgs');
				}
				
			}
		}
		elseif ($typeaction=='nocat') {
			
			if ($b_isAssortThirdParty) {
				require_once(DOL_DOCUMENT_ROOT."/product/class/product.class.php");
				
				$assort=new Assortment($db);
				$assort->label=$socid.'_NoCat_'.$productid;
				$assort->fk_soc=$socid;
				$assort->fk_prod=$idprod;
				$result=$assort->create($user);
				
				//Retreive product name
				$prod = new Product($db);
				$prod->fetch($idprod);
				
				if ($result > 0) {
					//creation OK
					setEventMessage($langs->trans("ProdWasAddedSuccessfully",$prod->ref.' '.$prod->libelle),'mesgs');
				} else {
					// Creation KO
					setEventMessage($langs->trans("ProdWasNotAddedSuccessfully",$prod->ref.' '.$prod->libelle),'errors');
				}
			}
			
			if ($b_isAssortProd) {
				require_once(DOL_DOCUMENT_ROOT."/societe/class/societe.class.php");
				
				$assort=new Assortment($db);
				$assort->label=$socid.'_NoCat_'.$prodid;
				$assort->fk_soc=$socid;
				$assort->fk_prod=$prodid;
				$result=$assort->create($user);
				
				//Retreive thirdparty name
				$Customer = new Societe($db);
				$Customer->fetch($socid);
				
				if ($result > 0)
				{
					//creation OK
					setEventMessage($langs->trans("ProdWasAddedSuccessfullyToCust",$Customer->nom),'mesgs');
				}
				else
				{
					// Creation KO
					setEventMessage($langs->trans("CustomerWasNotAddedSuccessfully",$Customer->nom),'errors');
				}
			}
		}
	}
	else {
		setEventMessage($lang->trans("UserNotAllowAdd"),'errors');
	}	
}
if ($action == "remove") {
	if ($user->rights->assortment->supprimer) {
		// Remove all link by category
		if (!empty($typeaction) && ($conf->global->ASSORTMENT_BY_CAT == 1)) { 
			
			if (($typeaction=="RemoveCatProd") && (!empty($catMereRemProd)) && (!empty($socid))) {
				$assort = new Assortment($db);
				$catToRemove=$catMereRemProd;
				
				$assort->fk_soc=$socid;
				
				//Remove all item of the category
				$result=$assort->remove_category($user,$catToRemove,$typeaction);
				
				//Retreive category name
				require_once(DOL_DOCUMENT_ROOT."/categories/class/categorie.class.php");
				$cat = new Categorie($db);
				$cat->fetch($catToRemove);
				
				if ($result==1)	{
					setEventMessage($langs->trans("CatWasRemove",$cat->label),'mesgs');
				}
				else {
					setEventMessage($langs->trans("CatWasNotRemove",$cat->label),'errors');
				}
			}
			if (($typeaction=="RemoveCatCustomer") && (!empty($catMereRemCust)) && (!empty($prodid))) {
				$assort = new Assortment($db);
				$catToRemove=$catMereRemCust;
				
				$assort->fk_prod=$prodid;
				
				//Remove all item of the category
				$result=$assort->remove_category($user,$catToRemove,$typeaction);
				
				//Retreive category name
				require_once(DOL_DOCUMENT_ROOT."/categories/class/categorie.class.php");
				$cat = new Categorie($db);
				$cat->fetch($catToRemove);
				
				if ($result==1)	{
					setEventMessage($langs->trans("CatWasRemove",$cat->label),'mesgs');
				}
				else {
					setEventMessage($langs->trans("CatWasNotRemove",$cat->label),'errors');
				}
			}
			
			if (($typeaction=="RemoveCatCustomer") && (!empty($catMereRemSupp)) && (!empty($type)) && (!empty($prodid))) {
				$assort = new Assortment($db);
				$catToRemove=$catMereRemSupp;
				
				$assort->fk_prod=$prodid;
				
				//Remove all item of the category
				$result=$assort->remove_category($user,$catToRemove,$typeaction);
				
				//Retreive category name
				require_once(DOL_DOCUMENT_ROOT."/categories/class/categorie.class.php");
				$cat = new Categorie($db);
				$cat->fetch($catToRemove);
				
				if ($result==1)	{
					setEventMessage($langs->trans("CatWasRemove",$cat->label),'mesgs');
				}
				else {
					setEventMessage($langs->trans("CatWasNotRemove",$cat->label),'errors');
				}
			}
		}
		else {
			// Remove only one assortment link
			if (!empty($id))
			{
				$assort = new Assortment($db);
				$result=$assort->fetch($id);
				// if id still exits
				if ($result!=0)	{
					$prodassortDelId=$assort->fk_prod;
					$custassortDelId=$assort->fk_soc;
					
					$result=$assort->delete($user);
					if ($result==1)	{
						if ($b_isAssortThirdParty)	{
							require_once(DOL_DOCUMENT_ROOT."/product/class/product.class.php");
							
							//Retreive product name
							$prod = new Product($db);
							$prod->fetch($prodassortDelId);
							
							setEventMessage($langs->trans("ProdWasRemove",$prod->ref.' '.$prod->libelle),'mesgs');
						}
						if ($b_isAssortProd) {
							require_once(DOL_DOCUMENT_ROOT."/societe/class/societe.class.php");
							
							//Retreive product name
							$cust = new Societe($db);
							$cust->fetch($custassortDelId);
							
							setEventMessage($langs->trans("ProdWasRemoveCust",$cust->nom),'mesgs');
						}
					}else {
						setEventMessage($langs->trans("ProdWasNotRemove",$prod->ref.' '.$prod->libelle),'errors');
					}
				}
			}
		}
	}
	else {
		setEventMessage($lang->trans("UserNotAllowRemove"),'errors');
	}
	
}

/***************************************************
* PAGE
*
* Put here all code to build page
****************************************************/
$html = new Form($db);

if ($b_isAssortThirdParty) {
	$langs->load("companies");
    require_once(DOL_DOCUMENT_ROOT."/core/lib/company.lib.php");
	require_once(DOL_DOCUMENT_ROOT."/societe/class/societe.class.php");
	
	$soc = new Societe($db);
	$result = $soc->fetch($socid);
	
	llxHeader("","",$langs->trans("Assortment"));

	/*
	 * Affichage onglets
	 */
	$head = societe_prepare_head($soc);

	dol_fiche_head($head, 'tabAssortment', $langs->trans("ThirdParty"),0,'company');

	print '<table class="border" width="100%">';

	print '<tr><td width="25%">'.$langs->trans("Name").'</td><td colspan="3">';
	print $html->showrefnav($soc,'socid','',($user->societe_id?0:1),'rowid','nom','','&type='.$type);
	print '</td></tr>';

	print '<tr><td>'.$langs->trans('Prefix').'</td><td colspan="3">'.$soc->prefix_comm.'</td></tr>';

	if ($soc->client)
	{
		print '<tr><td>';
		print $langs->trans('CustomerCode').'</td><td colspan="3">';
		print $soc->code_client;
		if ($soc->check_codeclient() <> 0) print ' <font class="error">('.$langs->trans("WrongCustomerCode").')</font>';
		print '</td></tr>';
	}

	if ($soc->fournisseur)
	{
		print '<tr><td>';
		print $langs->trans('SupplierCode').'</td><td colspan="3">';
		print $soc->code_fournisseur;
		if ($soc->check_codefournisseur() <> 0) print ' <font class="error">('.$langs->trans("WrongSupplierCode").')</font>';
		print '</td></tr>';
	}

	if ($conf->global->MAIN_MODULE_BARCODE)
	{
		print '<tr><td>'.$langs->trans('Gencod').'</td><td colspan="3">'.$soc->gencod.'</td></tr>';
	}

	// Address
	print "<tr><td valign=\"top\">".$langs->trans('Address')."</td><td colspan=\"3\">".nl2br($soc->address)."</td></tr>";

	// Zip / Town
	print '<tr><td width="25%">'.$langs->trans('Zip').'</td><td width="25%">'.$soc->cp."</td>";
	print '<td width="25%">'.$langs->trans('Town').'</td><td width="25%">'.$soc->ville."</td></tr>";

	// Country
	if ($soc->pays) {
		print '<tr><td>'.$langs->trans('Country').'</td><td colspan="3">';
		$img=picto_from_langcode($soc->pays_code);
		print ($img?$img.' ':'');
		print $soc->pays;
		print '</td></tr>';
	}

	// Phone
	print '<tr><td>'.$langs->trans('Phone').'</td><td>'.dol_print_phone($soc->tel,$soc->pays_code,0,$soc->id,'AC_TEL').'</td>';
	print '<td>'.$langs->trans('Fax').'</td><td>'.dol_print_phone($soc->fax,$soc->pays_code,0,$soc->id,'AC_FAX').'</td></tr>';

	// EMail
	print '<tr><td>'.$langs->trans('EMail').'</td><td>';
	print dol_print_email($soc->email,0,$soc->id,'AC_EMAIL');
	print '</td>';

	// Web
	print '<td>'.$langs->trans('Web').'</td><td>';
	print dol_print_url($soc->url);
	print '</td></tr>';

	// Assujeti a TVA ou pas
	print '<tr>';
	print '<td nowrap="nowrap">'.$langs->trans('VATIsUsed').'</td><td colspan="3">';
	print yn($soc->tva_assuj);
	print '</td>';
	print '</tr>';

	print '</table>';

	print '</div>';

	ManageAssortment($db,$soc,1);
		
	DisplayAssortment($db,$soc->id,1);
	
	
}
if ($b_isAssortProd)
{
	$langs->load("product");

	/*
	 * card Assortment of Product
	 */

	require_once(DOL_DOCUMENT_ROOT."/core/lib/product.lib.php");
	require_once(DOL_DOCUMENT_ROOT."/product/class/product.class.php");

	//Product
	$product = new Product($db);
	$result = $product->fetch(GETPOST("prodid",'int'),GETPOST("ref",'alpha'));


	llxHeader("","",$langs->trans("CardProduct".$product->type));

	$head=product_prepare_head($product, $user);
	$titre=$langs->trans("CardProduct".$product->type);
	$picto=($product->type==1?'service':'product');
	dol_fiche_head($head, 'tabAssortment', $titre,0,$picto);


	print '<table class="border" width="100%">';

	// Ref
	print "<tr>";
	print '<td width="15%">'.$langs->trans("Ref").'</td><td>';
	print $html->showrefnav($product,'ref','',1,'ref','ref','','&type='.$type);
	print '</td>';
	print '</tr>';

	// Label
	print '<tr><td>'.$langs->trans("Label").'</td><td>'.$product->libelle.'</td>';
	print '</tr>';

	// Status (to sell)
	print '<tr><td>'.$langs->trans("Status").' ('.$langs->trans("Sell").')'.'</td><td>';
	print $product->getLibStatut(2,0);
	print '</td></tr>';

	// Status (to buy)
	print '<tr><td>'.$langs->trans("Status").' ('.$langs->trans("Buy").')'.'</td><td>';
	print $product->getLibStatut(2,1);
	print '</td></tr>';

	print '</table>';

	print '</div>';

	if ($mesg) print dol_htmloutput_mesg($mesg);

	ManageAssortment($db,$product,0);
	ManageAssortment($db,$product,2);

	if ($mesgErr!='')
	{
		print get_htmloutput_errors($mesgErr);
	}
	
	DisplayAssortment($db,$product->id,0);

}

// End of page
$db->close();
llxFooter();
<?php
/* Copyright (C) 2011-2013 Florian HENRY  <florian.henry@open-concept.pro>
 *
 * Code of this page is mostly inspired from module category
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/**
 *  \file       htdocs/assortment/lib/assortment.lib.php
 *  \ingroup    crm
 *  \brief      Assortment Library pages
 *  \version    $Id: assortment.php,v 1.0 2011/01/01 eldy Exp $
 */
dol_include_once('/assortment/class/html.formassortment.class.php'); 

/**
 * Construct the form to manage assortment
 *
 *  @param	Database	$db			database identifier
 *  @param	int			$object		object identifier
 *  @param	string		$typeid		1=manage assortment for third party, 2 or 3 manage assortment from product
 *  @return void      		   	 Tabs array
 */
function ManageAssortment($db,$object,$typeid)
{
	$htmlassort = new FormAssortment($db);
	$htmlassort->form_manage_assortment($db,$object,$typeid);
}

/**
 * Construct the display to manage assortment
 *
 *  @param	Database	$db			database identifier
 *  @param	int			$objectid	object identifier
 *  @param	string		$type		1=manage assortment for third party, 2 or 3 manage assortment from product
 *  @return void      		   	 Tabs array
 */
function DisplayAssortment($db,$objectid,$type)
{
	$htmlassort = new FormAssortment($db);
	$htmlassort->list_assortment($objectid,$type);
}

/**
 *  Prepare head
 *
 *  @return array      		   	 Tabs array
 */
function assortmentadmin_prepare_head()
{
	global $langs, $conf;

	$langs->load("assortment@assortment");

	$h = 0;
	$head = array();

	$head[$h][0] = dol_buildpath("/assortment/admin/admin_assortment.php",1);
	$head[$h][1] = $langs->trans("Settings");
	$head[$h][2] = 'settings';
	$h++;
	$head[$h][0] = dol_buildpath("/assortment/admin/about.php",1);
	$head[$h][1] = $langs->trans("About");
	$head[$h][2] = 'about';
	$h++;

	complete_head_from_modules($conf,$langs,$object,$head,$h,'assortement');

	return $head;
}

?>

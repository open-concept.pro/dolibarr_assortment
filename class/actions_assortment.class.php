<?php
/* 
 * Copyright (C) 2011-2013 Florian HENRY  <florian.henry@open-concept.pro>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/**
        \file       DOL_DOCUMENT_ROOT_ALT/assortment/class/dao_assortment.class.php
        \ingroup    assortment
        \brief      this file is use for hook in product selection in order module
		\version    $Id: dao_product_composition.class.php,v 1.1 2010/06/07 14:49:46 pit Exp $
		\author		HENRY Florian
        \class      dao_assortment
		\remarks	Initialy built by copy of dao_composition_product for hook of assortment in order context
*/

/*error_reporting(E_ALL);
ini_set('display_errors', true);
ini_set('html_errors', false);*/

$res=0;
if (! $res && file_exists("../../../main.inc.php")) $res=@include_once("../../../main.inc.php");
if (! $res && file_exists("../../main.inc.php")) $res=@include_once("../../main.inc.php");
if (! $res && file_exists("../main.inc.php")) $res=@include_once("../main.inc.php");
if (! $res) die("Include of main fails");

class ActionsAssortment
{
	private $db;							//!< To store db handler
	private $error;							//!< To return error code (or message)
	private $errors=array();
	public $module_number=50500;
	private $bufflevel;
    
    /**
     *      \brief      Constructor
     *      \param      DB      Database handler
     */
    function ActionsAssortment($DB) 
    {
        $this->db = $DB;
        ob_start();
        $bufflevel=ob_get_level();
        ob_implicit_flush(false);
        return 1;
    }

	/**
     *      \brief      display product selection from assortment
     */
	public function formCreateProductOptions($parameters, $object, $action, $hookInst)
	{
		global $conf,$buyer;
		
		if ($conf->global->MAIN_MODULE_ASSORTMENT==1 && $conf->global->ASSORTMENT_ON_ORDER==1)
		{
			require_once('html.formassortment.class.php');
			$htmlAssort = new FormAssortment($this->db);
			
			 //Find in the output buffer if we need to change it 
			$content		= ob_get_contents();
		
			if (preg_match('~BEGIN PHP TEMPLATE predefinedproductline~',$content))
			{
				if($conf->global->PRODUIT_MULTIPRICES)
				{
					$strInsert=$htmlAssort->select_produits_assort('','idprod','',$conf->product->limit_size,$buyer->price_level,1,2,'',0,(isset($object->socid)?$object->socid:0));
				}
				else
				{
					$strInsert=$htmlAssort->select_produits_assort('','idprod','',$conf->product->limit_size,0,1,2,'',0,(isset($object->socid)?$object->socid:0));
				}
				
				if ($conf->global->PRODUIT_USE_SEARCH_TO_SELECT)
		        { 		
		        	$regex			= '~\<input type="hidden" name="idprod".*input type="text".*id="search_idprod".*/\>~is';
		        }
		        else
		        {	
					$regex			= '~\<select class="flat" name="idprod" id="idprod"\>.*\<\/select\>~is';
		        }
		        
		        if (preg_match($regex,$content))
		        {
			        //Find into the output buffer the change to do 
					$content		= preg_replace($regex, $strInsert, $content); 
					
					ob_end_clean();
					print $content;
		        }
		        else
		        {
		        	//dol_syslog("ActionsAssortment::formCreateProductOptions witout change=".$content, LOG_DEBUG);
		        }
			}
		}
	}
	
	/**
	 *      \brief      display product selection from assortment
     */
	public function formCreateProductSupplierOptions($parameters, $object, $action, $hookInst)
	{
		global $conf,$buyer;
		
		if ($conf->global->MAIN_MODULE_ASSORTMENT==1 && $conf->global->ASSORTMENT_ON_ORDER_FOUR==1)
		{
			require_once('html.formassortment.class.php');
			$htmlAssort = new FormAssortment($this->db);
			
			$soc_id=0;
			if (!empty($object->fourn_id)){$soc_id=$object->fourn_id;}
			elseif (!empty($object->socid)){$soc_id=$object->socid;}
			
			 //Find in the output buffer if we need to change it 
			$content		= ob_get_contents();

			if (preg_match('~\<form id="addpredefinedproduct"~',$content) || preg_match('~\<form.*name="addline_predef".*addline~',$content))
			{
				$strInsert=$htmlAssort->select_produits_fournisseurs_assort($soc_id,'',$parameters['htmlname'],'',$parameters['filtre']);
				
				if ($conf->global->PRODUIT_USE_SEARCH_TO_SELECT)
		        { 		
		        	$regex			= '~\<input type="hidden" name="'.$parameters['htmlname'].'".*input type="text".*id="search_'.$parameters['htmlname'].'">\<br>~is';
		        }
		        else
		        {	
					$regex			= '~\<select class="flat" id="select'.$parameters['htmlname'].'" name="'.$parameters['htmlname'].'"\>.*\<\/select\>~is';
		        }
		        
		        if (preg_match($regex,$content))
		        {
			        //Find into the output buffer the change to do 
					$content		= preg_replace($regex, $strInsert, $content); 
					
					ob_end_clean();
					print $content;
		        }
		        else
		        {
		        	//dol_syslog("ActionsAssortment::formCreateProductSupplierOptions witout change=".$content, LOG_DEBUG);
		        }
			}
		}
	}
	
	/**
	 *      \brief      display compagny selection from assortment
     */
	public function formCreateThirdpartyOptions($parameters, $object, $action, $hookInst)
	{
		global $conf;
		
		if ($conf->global->MAIN_MODULE_ASSORTMENT==1 && $conf->global->ASSORTMENT_ON_ORDER_FOUR==1)
		{
			require_once('html.formassortment.class.php');
			$htmlAssort = new FormAssortment($this->db);
			
			 //Find in the output buffer if we need to change it 
			$content		= ob_get_contents();
		
			if (preg_match('~\<input type="hidden" name="action" value="updateprice"~',$content))
			{
				$strInsert=$htmlAssort->select_company_assort($parameters['selected'],$parameters['html_name'],$parameters['filtre'],$parameters['showempty'],0,0,$parameters['prod_id']);
			
				$regex			= '~\<select id="'.$parameters['html_name'].'" class="flat" name="'.$parameters['html_name'].'"\>.*\<\/select\>~is';
		    
		        if (preg_match($regex,$content))
		        {
			        //Find into the output buffer the change to do 
					$content		= preg_replace($regex, $strInsert, $content); 
					
					ob_end_clean();
					print $content;
		        }
		        else
		        {
		        	//dol_syslog("ActionsAssortment::formCreateThirdpartyOptions witout change=".$content, LOG_DEBUG);
		        }
			}
		}
	}

}
?>
Assortment module
=========

This module required Dolibarr >=3.3 stable installation

Utiliser pour définir un assortiments produits pour les client/fournisseur. En options : utilisation des catégories, limitation des produits proposé lors de la commandes/facturation a ces assortiments

Licence
-------
GPLv3 or (at your option) any later version.

See COPYING for more information.

Other Licences
--------------
Uses Michel Fortin's PHP Markdown Licensed under BSD to display this README.

Contact
--------
This module is developped by florian.henry@open-concept.pro
http://wiki.dolibarr.org/index.php/Module_Assortiment
